
<!-- vim-markdown-toc GFM -->
* [Description](#description)
* [Usage](#usage)
* [Setup](#setup)
  * [Requirements](#requirements)

<!-- vim-markdown-toc -->


## Description

Convert a collection of \*.psd images into an imposed comic zine PDF

## Usage


```sh
bundle exec rake zine:pipeline[bleed,dir]
```

To build and run from a docker container:

```sh
bundle exec rake docker:build
bundle exec rake docker:run
```

## Setup

```sh
bundle
```

### Requirements

* ruby 2.4+
* ImageMagick 6.x (tested w/6.9.9)
* img2pdf 0.3.1+
* pdflatex w/pdfpages
