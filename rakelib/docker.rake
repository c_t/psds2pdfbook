namespace :docker do

  CONTAINER_LABEL='psd2zinepdf'

  desc 'build docker container'
  task :build, [:docker_args] do |task, args|
    sh "docker build -t #{CONTAINER_LABEL} #{args.docker_args} ."
  end

  desc 'run docker container'
  task :run, [:docker_args] do |task, args|
    uid=Process.uid
    gid=Process.gid
    dir=Dir.pwd
    sh %(docker run --user="#{uid}:#{gid}" -v "#{dir}:/opt/psd2pdfbook" #{CONTAINER_LABEL} cd /opt/psd2pdfbook; bundle exec rake "zine:pipeline[#{args.docker_args || '0.125in'}]")
  end
end
