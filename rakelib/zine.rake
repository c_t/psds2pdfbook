# frozen_string_literal: true

# pipeline:
#
# Things we magically know:
#   - bleed size
#   - the psds are in page order
#
# [rake]
#   ---[directory of page .psds]-->
# [ImageMagick]
#   ---> Get stats (dimensions, DPI, unit)
#   ---> Convert to PNGs
#     - Sanitize filenames
#     - Trim binding-side bleed (so page spreads merge correctly)
#   ---[collection of .pngs]-->
# [img2pdf]
#    ---> convert pngs into PDF of single pages
#    ---[PDF of single pages]-->
# [pdfbook/]
#  ---> construct LaTex
#  ( ---> BONUS: add crop marks # (not using pdfbook if we do this) )
#  ---> Impose booklet
#  ---> output PDF
# [rasterize sample pages]
#  ---> visual tests; maybe we can automate some
#
# There might be a more elegant two-step way to do this:
#
#   https://tex.stackexchange.com/a/235584
#
# pdfpages LaTex documentation (see page 3):
#
#   http://ftp.math.purdue.edu/mirrors/ctan.org/macros/latex/contrib/pdfpages/pdfpages.pdf
#
# Settings used in Clip Studio:
#
#   bleed: 0.11in
#
# We don't use --trim, because we want to keep the rest of the bleed (to help
# with paper cutting)
#
# DONE: trim bleed for *only* inner border
# Solution:
#   - use imagemagick's convert to chop the binding margin's bleed
#
# Refs:
#  - https://tex.stackexchange.com/questions/75393/pgfpages-outer-border-instead-of-border-shrink?rq=1
#  - https://tex.stackexchange.com/questions/91250/how-to-position-pdf-with-includepdf-in-book-also-depending-on-even-odd-page?rq=1
#  - Possibily it should be phrased "Avoid margin between pages:"
#     - https://tex.stackexchange.com/questions/410311/arrange-6-pdf-pages-on-one-side
#
#
# TODO: next up, crop marks:
#
# - https://tex.stackexchange.com/questions/175285/two-pages-on-one-and-exact-crop-marks
# - http://lilypond.1069038.n5.nabble.com/crop-marks-in-PDF-for-printing-td196055.html
# - http://www.texample.net/tikz/examples/modifying-current-page-node/
#
# TODO? : Offsetting pages because the outermost sheet is wrapped more thickly:
# - https://tex.stackexchange.com/a/60993
#
#
# pdfpages documentation (see page 3):
#  http://ftp.math.purdue.edu/mirrors/ctan.org/macros/latex/contrib/pdfpages/pdfpages.pdf
#
# f28 (man page: --short-edge requires 'everyshi')
#
# https://tex.stackexchange.com/questions/75393/pgfpages-outer-border-instead-of-border-shrink?rq=1

require 'measured'
require 'fileutils'
require 'pry'

class PageStats
  attr_accessor :stats
  attr_reader :pages
  def initialize(page_files, bleed)
    _page_files = page_files.sort
    @bleed = Measured::Length.parse(bleed)
    @pages = identify(_page_files)
    # Currently, we assume that all pages will follow the dimensions of the
    # first page.
    @stats = @pages[_page_files.first]
  end

  def identify(page_files)
    pages_stats = {}
    page_files.each do |file|
      page_stats = {}
      page = `identify -format "%W %H %x %y %U\n" '#{file}'`
             .split("\n").first.split(' ')
      page_stats = {
        width: page[0].to_i,
        height: page[1].to_i,
        dpi_x: page[2].to_i,
        dpi_y: page[3].to_i,
        units: page[4]
      }
      if page_stats[:units] == 'PixelsPerInch'
        page_stats[:width_inches] = Measured::Length.new(
          page_stats[:width].to_f / page_stats[:dpi_x], :in
        )
        page_stats[:height_inches] = Measured::Length.new(
          page_stats[:height].to_f / page_stats[:dpi_y], :in
        )
      end
      if page_stats[:dpi_x] == page_stats[:dpi_y]
        page_stats[:dpi] = page_stats[:dpi_x]
      end
      page_stats[:bleed] = @bleed
      pages_stats[file] = page_stats
    end
    pages_stats
  end
end

class PsdsImposerPipeline
  include FileUtils

  attr_accessor :img_type
  def initialize(psd_files, bleed, add_crop_marks = true)
    psd_files.sort!
    @psd_files      = psd_files
    @bleed          = bleed
    @add_crop_marks = add_crop_marks
    @werk_dir       = '.werk'
    @werk_dirs      = {
      img: File.join(@werk_dir, '01-img'),
      trimmed_img: File.join(@werk_dir, '02-img-inner-margin-trimmed'),
      pdf_single: File.join(@werk_dir, '03-pdf-single'),
      pdf_impositioned: File.join(@werk_dir, '04-pdf-impositioned')
    }
    @werk_files = {}
    @img_type   = '.png'
  end

  # Cross-platform way of finding an executable in the $PATH.
  #
  #   which('ruby') #=> /usr/bin/ruby
  def which(cmd)
    exts = ENV['PATHEXT'] ? ENV['PATHEXT'].split(';') : ['']
    ENV['PATH'].split(File::PATH_SEPARATOR).each do |path|
      exts.each do |ext|
        exe = File.join(path, "#{cmd}#{ext}")
        return exe if File.executable?(exe) && !File.directory?(exe)
      end
    end
    nil
  end

  def run
    # [ImageMagick]
    #   ---> Get stats (dimensions, DPI, unit)
    @pages = PageStats.new(@psd_files, @bleed)

    #   ---> Convert to PNGs
    #     - Sanitize filenames
    #     - Trim binding-side bleed (so page spreads merge correctly)
    #   ---[collection of .pngs]-->
    rm_rf @werk_dirs[:img]
    mkdir_p @werk_dirs[:img]
    rm_rf @werk_dirs[:trimmed_img]
    mkdir_p @werk_dirs[:trimmed_img]
    n = 0
    @pages.pages.each do |page, stats|
      n += 1

      # left-to-right booklet
      binding_side = if n.odd?
                       'West'
                     else
                       'East'
                     end

      img = File.basename(page.tr(' ', '_'), '.psd') + @img_type
      stats[:img_file] = File.join(@werk_dirs[:img], img)
      stats[:trimmed_img_file] = File.join(@werk_dirs[:trimmed_img], img)
      sh "convert '#{page}[0]' #{stats[:img_file]}"
      bleed_pixel_width = (stats[:bleed].value * stats[:dpi_x]).to_i
      sh "convert -gravity #{binding_side} -chop #{bleed_pixel_width}x0 #{stats[:img_file]} #{stats[:trimmed_img_file]}"
      Dir.chdir File.dirname(page) do
        sh "identify '#{File.basename page}'"
      end
      Dir.chdir File.dirname(stats[:img_file]) do
        sh "identify '#{File.basename stats[:img_file]}'"
      end
      Dir.chdir File.dirname(stats[:trimmed_img_file]) do
        sh "identify '#{File.basename stats[:trimmed_img_file]}'"
      end
    end

    # [img2pdf]
    rm_rf @werk_dirs[:pdf_single]
    mkdir_p @werk_dirs[:pdf_single]
    out_file = File.join(@werk_dirs[:pdf_single], 'single-pages.pdf')
    page_imgs_string = @pages.pages.map { |_p, s| s[:trimmed_img_file] }.join(' ')
    sh "img2pdf -o '#{out_file}' #{page_imgs_string}"
    @werk_files[:single_pages_pdf] = out_file

    #    ---> convert pngs into PDF of single pages
    #    ---[PDF of single pages]-->
    # [pdfbook/]
    rm_rf @werk_dirs[:pdf_impositioned]
    mkdir_p @werk_dirs[:pdf_impositioned]
    out_file = File.join(@werk_dirs[:pdf_impositioned], 'impositioned-booklet.pdf')

    trim_width = (@pages.stats[:width_inches] + @pages.stats[:width_inches]) - \
                 (@pages.stats[:bleed] + @pages.stats[:bleed])
    trim_width_s = "#{trim_width.value.to_f}#{trim_width.unit.name}"
    trim_height = @pages.stats[:height_inches] - \
                  (@pages.stats[:bleed] + @pages.stats[:bleed])

    trim_height_s = "#{trim_height.value.to_f}#{trim_height.unit.name}"

   tex = File.read File.expand_path('lib/files/pdfzine.template.tex')
   newtex = tex.gsub('__SRC_PDF__',@werk_files[:single_pages_pdf])
               .gsub('__TRIM_W__',trim_width_s)
               .gsub('__TRIM_H__',trim_height_s)
               .gsub('__PAPER_SIZE__','letter')
   newtex_file = File.join(@werk_dirs[:pdf_impositioned], 'zine.tex')
   File.open(newtex_file,'w'){|f| f.puts newtex }

  #sh %(pdflatex --output-directory "#{@werk_files[:pdf_impositioned]}" --jobname "impositioned-booklet" "#{newtex_file}")
  sh %(pdflatex  --jobname "impositioned-booklet" "#{newtex_file}")
  mv Dir["impositioned-booklet.*"], @werk_dirs[:pdf_impositioned]

    #  ---> output PDF
    # [rasterize sample pages]
    #  ---> visual tests; maybe we can automate some
    sh "xdg-open '#{out_file}'" if which('xdg-open')
  end
end

BLEED_DEFAULT = '0.125in'
ADD_CROP_MARKS = 'yes'

namespace :zine do
  desc <<-EOF.gsub(/^  /, '')
    Convert a directory of *.psds into an imposed comic zine PDF

    Task arguments:

    * bleed  Size of inner margin bleed to trim
             [default: '#{BLEED_DEFAULT}']

    * dir    Directory of .psd files to process
             [default: '#{Dir.pwd}']

    * marks  Add crop marks ('yes' or 'no')
             [default: '#{ADD_CROP_MARKS}']
  EOF
  task 'pipeline', [:bleed, :dir, :marks] do |_task, args|
    args.with_defaults(
      bleed: BLEED_DEFAULT,
      dir:   Dir.pwd,
      marks: ADD_CROP_MARKS
    )
    psds = Dir[File.join(args.dir, '*.psd')]
    pipeline = PsdsImposerPipeline.new(psds, args.bleed)
    pipeline.img_type = '.tif'
    pipeline.run
  end
end
