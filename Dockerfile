FROM fedora:28

RUN dnf install -y which procps
RUN dnf install -y \
  autoconf \
  automake \
  bison \
  bzip2 \
  gcc-c++ \
  libffi-devel \
  libtool \
  libyaml-devel \
  make \
  openssl-devel \
  patch \
  readline-devel \
  sqlite-devel \
  zlib-devel

RUN gpg2 --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB

RUN \curl -sSL https://get.rvm.io | bash -s stable
RUN /bin/bash --login -c "rvm install 2.4"
RUN /bin/bash --login -c "rvm use 2.4 --default"
RUN /bin/bash --login -c "gem install bundler"

RUN dnf install -y \
  ImageMagick \
  python3-img2pdf \
  texlive-pdfjam.noarch

RUN mkdir -p /opt/psd2pdfbook
COPY Gemfile Rakefile rakelib /opt/psd2pdfbook/
RUN chmod -R 0755 /opt/psd2pdfbook
RUN /bin/bash --login -c "cd /opt/psd2pdfbook && bundle"
VOLUME ["/opt/psd2pdfbook/psds"]
RUN chmod -R 0755 /opt/psd2pdfbook


